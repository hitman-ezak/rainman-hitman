<?php get_header() ?>

	<div class="ordering">
		<h1>Оформление заказа</h1>
		<div class="inner-l">
			<form action="">
				<div class="form-box-short underline">

					<div class="box-title">
						Способ доставки
					</div>
					<div class="row">
						<input type="radio" id="courier" name="delivery-method"/>
						<label for="courier" class="up">Курьер</label>
					</div>
					<div class="row">
						<input type="radio" id="ukrpost" name="delivery-method"/>
						<label for="ukrpost" class="up">Укрпочта</label>
					</div>
					<div class="row">
						<input type="radio" id="newpost" name="delivery-method"/>
						<label for="newpost" class="up">Доставка Новой Почтой</label>
					</div>
					<div class="row">
						<input type="radio" id="dhl" name="delivery-method"/>
						<label for="dhl" class="up">Доставка DHL</label>
					</div>
				</div>

				<div class="form-box underline">
					<div class="box-title">
						Информация о получателе
					</div>
					<div class="row">
						<label for="firstname">Имя:</label>
						<input type="text" id="firstname" name="delivery-method"/>
					</div>
					<div class="row">
						<label for="lastname">Фамилия:</label>
						<input type="text" id="lastname" name="delivery-method"/>
					</div>
					<div class="row">
						<label for="patronymic">Отчество:</label>
						<input type="text" id="patronymic" name="delivery-method"/>
					</div>
					<div class="row">
						<label for="phone">Телефон:</label>
						<input type="text" id="phone" name="delivery-method"/>
					</div>
				</div>

				<div class="form-box">
					<div class="box-title">
						Информация о получателе
					</div>
					<div class="row">
						<label for="city">Населенный пункт:</label>
						<input type="text" id="city" name="delivery-method"/>
					</div>
					<div class="row">
						<label for="vilage">Улица:</label>
						<input type="text" id="vilage" name="delivery-method"/>
					</div>
					<div class="row">
						<label for="house">Дом:</label>
						<input type="text" id="house" name="delivery-method"/>
					</div>
					<div class="row">
						<label for="flat">Квартира:</label>
						<input type="text" id="flat" name="delivery-method"/>
					</div>
				</div>
				<input type="submit" id="order-submit" value="Оформить заказ"/>
			</form>
		</div>
		<div class="inner-r">
			<div class="order-item">
				<table>
					<tr>
						<td>
							<div class="item-foto">
								<img src="http://rainman.futurity.pro/wp-content/uploads/gallery/small_order/man3_small_order.png" alt=""/>
							</div>
						</td>
						<td>
							<div class="item-descr">
								<div class="item-title">
									<a href="#">Джинсы серые с эффектом потертых - 27-32</a>
								</div>
                            <span class="item-size">
                                22-37
                            </span>
                            <span class="item-num">
                                <select name="num">
									<option value="1">1 шт.</option>
									<option value="2">2 шт.</option>
									<option value="3">3 шт.</option>
									<option value="4">4 шт.</option>
								</select>
                            </span>
                            <span class="item-prize">
                                500грн.
                            </span>
							</div>
						</td>
						<td>
							<div class="close">&nbsp;</div>
						</td>
					</tr>
				</table>
			</div>
			<div class="order-item">
				<table>
					<tr>
						<td>
							<div class="item-foto">
								<img src="http://rainman.futurity.pro/wp-content/uploads/gallery/small_order/man3_small_order.png" alt=""/>
							</div>
						</td>
						<td>
							<div class="item-descr">
								<div class="item-title">
									<a href="#">Джинсы серые с эффектом потертых - 27-32</a>
								</div>
                            <span class="item-size">
                                22-37
                            </span>
                            <span class="item-num">
                                <select name="num">
									<option value="1">1 шт.</option>
									<option value="2">2 шт.</option>
									<option value="3">3 шт.</option>
									<option value="4">4 шт.</option>
								</select>
                            </span>
                            <span class="item-prize">
                                500грн.
                            </span>
							</div>
						</td>
						<td>
							<div class="close">&nbsp;</div>
						</td>
					</tr>
				</table>
			</div>
			<div class="paiment-result">
				<p>
					Сумма к оплате: 1000 грн.
				</p>
			</div>
		</div>
	</div>
<?php get_footer() ?>
