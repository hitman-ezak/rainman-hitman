</div>
</div>
<div id="footer">
    <div class="footer-in">
        <div class="cont-in">
            <div class="footer-social-links">
                <div class="title">Будь на связи</div>
                <ul class="social-links">
                    <?php if($option = synved_option_get('options', 'vk')): ?>
                    <li><a href="<?php echo $option ?>"><span class="social1"></span><span class="social1-1"></span></a></li>
                    <?php endif; ?>
                    <?php if($option = synved_option_get('options', 'odnoklasniki')): ?>
                    <li><a href="<?php echo $option ?>" ><span class="social2"></span><span class="social2-2"></span></a></li>
                    <?php endif; ?>
                    <?php if($option = synved_option_get('options', 'facebook')): ?>
                    <li><a href="<?php echo $option ?>" ><span class="social3"></span><span class="social3-3"></span></a></li>
                    <?php endif; ?>
                    <?php if($option = synved_option_get('options', 'twitter')): ?>
                    <li><a href="<?php echo $option ?>" ><span class="social4"></span><span class="social4-4"></span></a></li>
                    <?php endif; ?>
                    <?php if($option = synved_option_get('options', 'instagram')): ?>
                    <li><a href="<?php echo $option ?>" ><span class="social5"></span><span class="social5-5"></span></a></li>
                    <?php endif; ?>
                    <?php if($option = synved_option_get('options', 'google')): ?>
                    <li><a href="<?php echo $option ?>" ><span class="social6"></span><span class="social6-6"></span></a></li>
                    <?php endif; ?>
                </ul>
            </div>
            <a href="#" id="to-top"></a>
        </div>
    </div>
</div>
<div class="pay-block">
    <div class="cont-in">
        <!--div class="pay">
            <a class="submit submit2" href="/order/">Купить</a>
        </div-->
        <a href="#" class="pay-logo"></a>
        <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'pay-nav' ) ); ?>
    </div>
</div>
</div>

<?php wp_footer(); ?>

</body>
</html>