<?php get_header(); ?>
    <div class="reviews">
        <?php $args = array( 'post_type' => 'reviews','orderby' => 'date', 'order' => 'DESC' );
        $review = new WP_Query( $args );
        while ( $review->have_posts() ) : $review->the_post();
            $rev_image_id = get_post_thumbnail_id($post->ID);
            $rev_url = wp_get_attachment_url( $rev_image_id );
            $RevUrlThumb = wp_get_attachment_image_src( $rev_image_id);
            $RevUrlThumb = $RevUrlThumb[0];
            $rev_url = get_post_meta( $post->ID, 'url', true );?>
            <div class="review">
                <div class="rev-thumb"><img src="<?php echo $RevUrlThumb?>" /></div>
                <div class="entry-content">
                    <?php
                    the_content();
                    if($rev_url != ''){?>
                        <a href="<?php echo $rev_url[0]["url"]?>"><?php the_title();?></a>
                    <?php } else {
                        the_title();
                    }?>
                </div>
            </div>
        <?php endwhile;?>
    </div>
<?php get_footer(); ?>