<?php get_header() ?>

<div class="shape">
    <ul>
        <li>
            <img src="http://rainman-wp/wp-content/uploads/gallery/man1.png" alt=""/>
            <div class="shape-price">
                <p>650 грн.</p>
            </div>
            <div class="description">
                <div class="shape-title">
                    <p>Джинсы очень крутые</p>
                </div>
                <div class="size">
                    <p>
                        Размеры:
                        <a href="#">33-32</a>
                    </p>
                </div>
            </div>
        </li>
        <li>
            <img src="http://rainman-wp/wp-content/uploads/gallery/man2.png" alt=""/>
            <div class="shape-price">
                <p>650 грн.</p>
            </div>
            <div class="description">
                <div class="shape-title">
                    <p>Джинсы светлые</p>
                </div>
                <div class="size">
                    <p>
                        Размеры:
                        <a href="#">33-32</a>
                    </p>
                </div>
            </div>
        </li>
        <li>
            <img src="http://rainman-wp/wp-content/uploads/gallery/man3.png" alt=""/>
            <div class="shape-price">
                <p>650 грн.</p>
            </div>
            <div class="description">
                <div class="shape-title">
                    <p>Джинсы темные</p>
                </div>
                <div class="size">
                    <p>
                        Размеры:
                        <a href="#">33-32</a>
                    </p>
                </div>
            </div>
        </li>
        <li>
            <img src="http://rainman-wp/wp-content/uploads/gallery/man4.png" alt=""/>
            <div class="shape-price">
                <p>650 грн.</p>
            </div>
            <div class="description">
                <div class="shape-title">
                    <p>Джинсы очень крутые</p>
                </div>
                <div class="size">
                    <p>
                        Размеры:
                        <a href="#">33-32</a>
                    </p>
                </div>
            </div>
        </li>
        <li>
            <img src="http://rainman-wp/wp-content/uploads/gallery/man5.png" alt=""/>
            <div class="shape-price">
                <p>650 грн.</p>
            </div>
            <div class="description">
                <div class="shape-title">
                    <p>Джинсы светлые</p>
                </div>
                <div class="size">
                    <p>
                        Размеры:
                        <a href="#">33-32</a>
                    </p>
                </div>
            </div>
        </li>
        <li>
            <img src="http://rainman-wp/wp-content/uploads/gallery/man6.png" alt=""/>
            <div class="shape-price">
                <p>650 грн.</p>
            </div>
            <div class="description">
                <div class="shape-title">
                    <p>Джинсы темные</p>
                </div>
                <div class="size">
                    <p>
                        Размеры:
                        <a href="#">33-32</a>
                    </p>
                </div>
            </div>
        </li>
    </ul>
</div>
<div class="pager">
    <ul>
        <li class="prev">
            <a href="#"><img src="http://rainman-wp/wp-content/themes/futurity-rainman/images/icons/prev.png" alt=""/>Предыдущая</a>
        </li>
        <li class="after-prev">Страница 1 из 3</li>
        <li class="next">
            <a href="#">Следующая <img src="http://rainman-wp/wp-content/themes/futurity-rainman/images/icons/next.png" alt=""/></a>
        </li>
    </ul>
</div>

<?php get_footer() ?>
