jQuery(document).ready(function() {


    jQuery('#camera_wrap_1').camera({
        thumbnails: true
    });
    $("ul.tabs").tabs("div.panes > div");
    /*jQuery('#camera_wrap_2').camera({
        height: '400px',
        loader: 'bar',
        pagination: false,
        thumbnails: true
    });*/

    $(window).scroll(function () {
        if ($(this).scrollTop() > 0) {
            $('#to-top').fadeIn();
        } else {
            $('#to-top').fadeOut();
        }
    });

    $('#to-top').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 1500);
        return false;
    });



    jQuery(".form input").click(function()
    {
        jQuery(this).toggleClass('active').find(".form input").slideToggle(500);
        return false;
    });

    jQuery(".form textarea").click(function()
    {
        jQuery(this).toggleClass('active').find(".form textarea").slideToggle(500);
        return false;
    });

    var tag = document.createElement('script');

    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    var player;
    function onYouTubeIframeAPIReady() {
        player = new YT.Player('player', {
            height: '300',
            width: '492',
            videoId: 'M7lc1UVf-VE',
            events: {
                'onReady': onPlayerReady,
                'onStateChange': onPlayerStateChange
            }
        });
    }

    function onPlayerReady(event) {
        event.target.playVideo();
    }

    var done = false;
    function onPlayerStateChange(event) {
        if (event.data == YT.PlayerState.PLAYING && !done) {
            setTimeout(stopVideo, 6000);
            done = true;
        }
    }

    function stopVideo() {
        player.stopVideo();
    }

    $(window).scroll(function () {
        if ($(this).scrollTop() > 300) {
            $('.pay-block').fadeIn();
        } else {
            $('.pay-block').fadeOut();
        }
    });

    $('#menu-item-18 a').addClass('active');

    $('.ffCheckboxWrapper').click(function(){
        if ($(this).hasClass('on')) {
           $(this).find('input').attr('checked', 'checked');
        } else {
            $(this).find('input').removeAttr('checked');
        }
    })


    if($('#order').length) {
        $('#order').submit(function(){
            var errors = 0;
            $('#errors').html('');
            $('.error').removeClass('error');
            $('.required').each(function(){
                var val = $(this).val();
                var name = $(this).attr('name');
                if ($(this).attr('type') == 'radio') {
                    if($('input[name='+name+']:checked').length > 0) {
                        val = $('input[name='+name+']:checked').val();
                    } else {
                        val = '';
                    }
                }

                if ($(this).attr('type') == 'checkbox') {
                    if($('input[name='+name+']').attr('checked') == 'checked') {
                        val = "yes";
                    } else {
                        val = '';
                        $(this).parent().addClass('error');
                    }
                }

                if (val.length == 0) {
                    var label = $(this).data('label')
                    if(!label)
                        label = name;

                    $(this).addClass('error');
                    $(this).parents('fieldset').addClass('error');
                    errors++;
                    var error = '<li>Заполните обязательное поле ' + label + '</li>';
                    $('#errors').append(error);
                }
            });

            if (errors > 0)
                return false


        });
    }

});