<?php
CONST CONSTRUCTOR_JEANS_ITEM_ID = 266; // id фейкового товара в конструкторе джинсов

// Storing processing form in session via ajax request
if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
	if(!empty($_POST['CheckoutForm'])) {

		$params = array();
		parse_str($_POST['CheckoutForm'], $params);
		unset($params['operation_xml']);
		unset($params['signature']);

		//$params['delivery-method'] = setDeliveryMethod($params['delivery-method']);
		$_SESSION['CheckoutForm'] = $params['CheckoutForm'];

		echo json_encode('ok');
		return;
	}
}

// Action for testing mail
if(!empty($_GET['test'])){
	$xml = '<response>
		  <version>1.2</version>
		  <merchant_id></merchant_id>
		  <order_id> ORDER_123456</order_id>
		  <amount>1.01</amount>
		  <currency>UAH</currency>
		  <description>Comment</description>
		  <status>success</status>
		  <code></code>
		  <transaction_id>31</transaction_id>
		  <pay_way>card</pay_way>
		  <sender_phone>+3801234567890</sender_phone>
		  <goods_id>266</goods_id>
		  <pays_count>5</pays_count>
		</response>';

	//$xml = $_POST['operation_xml'];

	$dom = new domDocument;
	$dom->loadXML($xml);
	$s = simplexml_import_dom($dom);

	$dom = new domDocument;
	$dom->loadXML($xml);
	$s = simplexml_import_dom($dom);

	if ($s->status == 'success') {

		// У кастомных джинсов должен быть id = 1
		$CheckoutForm = $_SESSION['CheckoutForm'];

		$message = "<h1>Заказ на джинсы Rain Man</h1><br>
			<br>
			Номер заказа : {$s->order_id}<br>
			Оплаченная сумма : {$s->amount} {$s->currency}<br>

			Имя:      {$CheckoutForm['name']}<br>
			Фамилия:  {$CheckoutForm['surname']}<br>
			Отчество: {$CheckoutForm['patronymic']}<br>
			Email:    {$CheckoutForm['email']}<br>
			Телефон:  {$CheckoutForm['phone']}<br>

			Город:    {$CheckoutForm['city']}<br>
			Улица:    {$CheckoutForm['street']}<br>
			Дом:      {$CheckoutForm['house']}<br>
			Квартира: {$CheckoutForm['flat']}<br>

			Способ доставки: {$CheckoutForm['delivery-method']}<br>";

		if ($s->goods_id  == CONSTRUCTOR_JEANS_ITEM_ID){
			$OrderForm = $_SESSION['OrderForm'];

			if(!empty($OrderForm['size-type'])){

				$message .= "<h1>Параметры заказа</h1><br>
				Тип размера:  {$OrderForm['size-type']}<br/>";

				if($OrderForm['size-type'] == 'Размер'){
				$message .= "
				Размер:  {$OrderForm['standard-size']}<br/>
				Стандарт размер:  {$OrderForm['size-standard']}<br/>";
				} else if ($OrderForm['size-type'] == 'По мерке'){
				$message .= "
				Объём талии:   {$OrderForm['row1']}     <br>
				Объём бёдер:   {$OrderForm['row2']}     <br>
				Обхват бедра:  {$OrderForm['row3']}     <br>
				Колено:        {$OrderForm['row4']}     <br>
				Икра:  		   {$OrderForm['row5']}     <br>

				Высота талии:  {$OrderForm['row6']}      <br>
				Пошаговый шов: {$OrderForm['row7']}      <br>
				Длина изделия: {$OrderForm['row8']}      <br>
				";
				}
				$message .= "
					Модель: {$OrderForm['model']}      		<br>
					Тип посадки: {$OrderForm['r1']}       <br>
					Ткань: {$OrderForm['cloth']}      		<br>
					Механические изменения: {$OrderForm['damages']}     <br>
				";
			} else {
				die($xml);
			}
		} else {
			$cartItems = $_SESSION['cartItems'];
			$message .= "<h1>Заказанные товары:</h1> <br/>";
			foreach($cartItems as $item) {
				$message .="Название: {$item['name']} <br/>
				" ;
				if (!empty($item['size'])) {
					$message .= "Размер: {$item['size']} <br/>";
				}
				$message .= "Цена: {$item['price']}<br/><hr/>";
			}
			$message .= "Общая сумма заказов: {$s->amount}";
		}

		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
		$headers .= 'From: Rain Man <order@rain-man.com.ua>' . "\r\n";

		mail($CheckoutForm['email'], 'Rain Man: Новый заказ', $message, $headers);
		mail(synved_option_get('options', 'email'), 'Rain Man: Новый заказ', $message, $headers);
		session_destroy();

		global $woocommerce;
		$woocommerce->cart->empty_cart();

	}

	header('location:/thankyou/');
	die();
}

// Processing response from liq pay
if(!empty($_GET['liqpay']) AND !empty($_POST['operation_xml '])) {
	//$xml = $_POST['operation_xml'];

	$dom = new domDocument;
	$dom->loadXML($xml);
	$s = simplexml_import_dom($dom);

	$dom = new domDocument;
	$dom->loadXML($xml);
	$s = simplexml_import_dom($dom);

	if ($s->status == 'success') {

		// У кастомных джинсов должен быть id = 1
		$CheckoutForm = $_SESSION['CheckoutForm'];

		$message = "<h1>Заказ на джинсы Rain Man</h1><br>
			<br>
			Номер заказа : {$s->order_id}<br>
			Оплаченная сумма : {$s->amount} {$s->currency}<br>

			Имя:      {$CheckoutForm['name']}<br>
			Фамилия:  {$CheckoutForm['surname']}<br>
			Отчество: {$CheckoutForm['patronymic']}<br>
			Email:    {$CheckoutForm['email']}<br>
			Телефон:  {$CheckoutForm['phone']}<br>

			Город:    {$CheckoutForm['city']}<br>
			Улица:    {$CheckoutForm['street']}<br>
			Дом:      {$CheckoutForm['house']}<br>
			Квартира: {$CheckoutForm['flat']}<br>

			Способ доставки: {$CheckoutForm['delivery-method']}<br>";

		if ($s->goods_id  == CONSTRUCTOR_JEANS_ITEM_ID){
			$OrderForm = $_SESSION['OrderForm'];

			if(!empty($OrderForm['size-type'])){

				$message .= "<h1>Параметры заказа</h1><br>
				Тип размера:  {$OrderForm['size-type']}<br/>";

				if($OrderForm['size-type'] == 'Размер'){
					$message .= "
				Размер:  {$OrderForm['standard-size']}<br/>
				Стандарт размер:  {$OrderForm['size-standard']}<br/>";
				} else if ($OrderForm['size-type'] == 'По мерке'){
					$message .= "
				Объём талии:   {$OrderForm['row1']}     <br>
				Объём бёдер:   {$OrderForm['row2']}     <br>
				Обхват бедра:  {$OrderForm['row3']}     <br>
				Колено:        {$OrderForm['row4']}     <br>
				Икра:  		   {$OrderForm['row5']}     <br>

				Высота талии:  {$OrderForm['row6']}      <br>
				Пошаговый шов: {$OrderForm['row7']}      <br>
				Длина изделия: {$OrderForm['row8']}      <br>
				";
				}
				$message .= "
					Модель: {$OrderForm['model']}      		<br>
					Тип посадки: {$OrderForm['r1']}       <br>
					Ткань: {$OrderForm['cloth']}      		<br>
					Механические изменения: {$OrderForm['damages']}     <br>
				";
			} else {
				die($xml);
			}
		} else {
			$cartItems = $_SESSION['cartItems'];
			$message .= "<h1>Заказанные товары:</h1> <br/>";
			foreach($cartItems as $item) {
				$message .="Название: {$item['name']} <br/>
				" ;
				if (!empty($item['size'])) {
					$message .= "Размер: {$item['size']} <br/>";
				}
				$message .= "Цена: {$item['price']}<br/><hr/>";
			}
			$message .= "Общая сумма заказов: {$s->amount}";
		}

		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
		$headers .= 'From: Rain Man <order@rain-man.com.ua>' . "\r\n";

		mail($CheckoutForm['email'], 'Rain Man: Новый заказ', $message, $headers);
		mail(synved_option_get('options', 'email'), 'Rain Man: Новый заказ', $message, $headers);
		session_destroy();

		global $woocommerce;
		$woocommerce->cart->empty_cart();

	}

	header('location:/thankyou/');
	die();
}

?>