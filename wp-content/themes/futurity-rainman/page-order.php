<?php const CONSTRUCTOR_JEANS_ITEM_ID = 266;
global $woocommerce;
$woocommerce->cart->empty_cart();
$woocommerce->cart->add_to_cart( CONSTRUCTOR_JEANS_ITEM_ID );
?>

<?php get_header(); ?>
<!-- action="<?php the_permalink();?> -->

<form id="order" method="POST" action="/processing">
<h1>Оформление заказа</h1>

<style>
	.popover-box {
		background: #fff;
		border: 1px solid #ababab;
		width: 258px;
		position: absolute;
		top: -23px;
		left: -268px;
		z-index: 999;
		border-radius: 5px;
	}
	.popover-box .popover-box__inner {
		padding: 10px;
		position: relative;
	}
	.popover-box .corner{
		background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAkAAAAQCAYAAAGzT2XVAAAACXBIWXMAAAsTAAALEwEAmpwYAAABNmlDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjarY6xSsNQFEDPi6LiUCsEcXB4kygotupgxqQtRRCs1SHJ1qShSmkSXl7VfoSjWwcXd7/AyVFwUPwC/0Bx6uAQIYODCJ7p3MPlcsGo2HWnYZRhEGvVbjrS9Xw5+8QMUwDQCbPUbrUOAOIkjvjB5ysC4HnTrjsN/sZ8mCoNTIDtbpSFICpA/0KnGsQYMIN+qkHcAaY6addAPAClXu4vQCnI/Q0oKdfzQXwAZs/1fDDmADPIfQUwdXSpAWpJOlJnvVMtq5ZlSbubBJE8HmU6GmRyPw4TlSaqo6MukP8HwGK+2G46cq1qWXvr/DOu58vc3o8QgFh6LFpBOFTn3yqMnd/n4sZ4GQ5vYXpStN0ruNmAheuirVahvAX34y/Axk/96FpPYgAAACBjSFJNAAB6JQAAgIMAAPn/AACA6AAAUggAARVYAAA6lwAAF2/XWh+QAAAB8UlEQVR42mK4du3a/88f39gw/v/////x40cYAAAAAP//Yvj25b0nw/////8DAAAA//9i/P////9r164xiIkKcDIxMDAwvHv3hoGRkZEDAAAA//8EwdEJACAIRdFrVB+2VPuPEIigg7zOscy3j/sySeqKa5IEMAC6Yn4AAAD//2L8/////+vXrzNIiAuafP/+45yggAAjXBkDAwPDtWvXGHi42fyZGJDA588fGcRERbcw/v////+xY0cYtLXUmP79+9fFxsp2HCCGI0cObXv75rnM////Gf7//8/A8P////+HDh14+///f4aHD26zMfyHgqNHD//6//8/A4qNx48e/otim4CQCDNc4Pr16wy8PBwFLDAOFydLChsb63ym69evM/BwswVKiIuvZWJi4gYI7rd3b16dkZQST+fn53/69evXv3LyKm8ePrjNzMzMzIli8/Xr1xnev3/LICkpFs3EyLiSj4+X8+fPX99RFCEHzLcvnxikpCVVGBkZ7mFVhKz4x4+v/5hwKbh+/TrDl08fGGRlpEzgwQMD165d+3/gwL7/L188tvj//z/D+3cvWODWQX3IIC0jVSMoyN/NwszCyMTExP7//38uxmvXrv1///Y1g5y8TCAzM/MmXh6e/99//FD7+fPnHz5e3q9fv33TAAwAH7sGWX6WhdQAAAAASUVORK5CYII=);
		width: 9px;
		height: 16px;
		position: absolute;
		right: -9px;
		top: 30px;
	}
	.popover-box .jeans-sizes-type tr th {
		width: 56px;
		padding: 0 8px;
		color: #0052b8;
		font-weight: normal;
		text-decoration: underline;
		border-bottom: 1px solid #ababab;
		padding-bottom: 6px;
	}
	.popover-box .jeans-size  .inseam {
		text-align: right;
		padding-right: 25px;
	}
	.popover-box .jeans-size tr th {
		font-weight: normal;
	}
	.popover-box .jeans-size tr td {
		padding-bottom: 6px;
		color: #0052b8;
		font-weight: normal;
	}
	.popover-box .jeans-size tr td {
		padding-left: 10px;
	}
	.popover-box .jeans-size tr th {
		color: #ababab;
		text-align: center;
		padding-top: 6px;
	}
	.popover-box .jeans-size tr td:hover,
	.popover-box .jeans-size .picked {
		background: #ababab;
		border-radius: 4px;
		color: #fff;
	}
	.popover-box table tr th.active {
		color: #2a2a2a;
	}
	.size-select-type.active{
		text-decoration: underline;
		color: #0052b8;
	}
</style>
<div class="center">
<ul class="tabs">
	<li>

			<label> <a class="size-select-type" href="standard-size">Размер</a> | <a class="size-select-type" href="custom-measure">По Мерке</a></label>
			<fieldset class="radio-type-size" style="display: none">
				<input type="radio" id="standard-size-radio" name="OrderForm[size-type]"  value="Размер" >
				<input type="radio" id="custom-measure-radio" checked="checked" name="OrderForm[size-type]" value="По мерке" >
			</fieldset>
			<script>
				$(function(){
					$('.size-select-type').click(function(e){
						e.preventDefault();
						$('.size-select-type').removeClass('active');
						$(this).addClass('active');
						var ShowContent = $(this).attr('href');

						// Hide or Show size field with pop-over
						if(ShowContent == 'standard-size'){
							$('#standard-size').show();
							$('.custom-measure input').removeClass('required');
							$('.custom-measure input').attr('disabled', true);
							$('.jeans-size-type-radio input').attr('disabled', false);
							$('.radio-type-size input').attr('disabled', false);
							$('#standard-size input').addClass('required');
						} else {
							$('#standard-size').hide();
							$('#standard-size input').removeClass('required');
							$('#standard-size input').attr('disabled', false);
							$('.jeans-size-type-radio input').attr('disabled', true);
							$('.radio-type-size input').attr('disabled', true);
							$('.custom-measure').find('input').addClass('required');
							$('.custom-measure input').attr('disabled', false);
						}
						var checkedRadio = ShowContent + '-radio';

						$('.radio-type-size input').attr('checked',false);
						$('#'+checkedRadio).attr('checked', true);

						$('.size-type').hide();
						$('.' + ShowContent).show();

					}).click();

					var popoverBox = $('.popover-box');
					
					$('#standard-size').click(function(){
						if (popoverBox.is(':hidden')) {
							popoverBox.show();
						} else {
							popoverBox.hide();
						}
					});
					$('.jeans-sizes-type th').click(function(){
						$('.jeans-size tr td').removeClass('picked');
						$('.jeans-sizes-type th').removeClass('active');
						$(this).addClass('active');
						$('.jeans-size').hide();

						var tableID		  = $(this).data('table');
						var sizeTypeRadio = $(this).data('table')+'-radio';

						$('.jeans-size-type-radio input').attr('checked', false);
						$(sizeTypeRadio).attr('checked', true);

						$(tableID).show();
						var td = $(tableID).find('tr').eq(1).children();
						td[0].click();
						td[1].click();

						return false;
					});
					$('.jeans-size tr td').click(function(){

						if($(this).hasClass('size')){
							$('.jeans-size tr .size').removeClass('picked');
							$(this).addClass('picked');
						}
						else if($(this).hasClass('inseam')){
							sizeAndSeam += $(this).text;
							$('.jeans-size tr .inseam').removeClass('picked');
							$(this).addClass('picked');
						}
						var jeansSize = $('.jeans-size tr .size.picked').text();
						var jeansInseam = $('.jeans-size tr .inseam.picked').text();

						var sizeAndSeam = jeansSize + ' ' + jeansInseam;
						$('input[name="OrderForm[standard-size]"]').val(sizeAndSeam);
					});
					$('.jeans-sizes-type tr th:first-child').click();
				});
			</script>
			<div class="inputfield size-type standard-size"  id="standard-size" style="position: relative">
				<div class="bgactive">
					<input type="text" class="inpfield required"  name="OrderForm[standard-size]" data-label="Размер" value=""/>
					<span class="b1"></span><span id=""></span></div>
				<div class="clear"></div>

				<!-- Модальное окно с выбором размера-->
				<div class="popover-box">
					<div class="popover-box__inner">
						<i class="corner"></i>
						<div style="display: none" class="jeans-size-type-radio">
							<input type="radio" id="us-sizes-0-20-radio" name="OrderForm[size-standard]" value="USA Size 0-20"/>
							<input type="radio" id="us-sizes-20-32-radio" name="OrderForm[size-standard]" value="USA Size 32-32"/>
							<input type="radio" id="eu-sizes-radio"name="OrderForm[size-standard]" value="EU Size"/>
						</div>
						<table class="jeans-sizes-type">
							<thead>
								<tr>
									<th data-table="#us-sizes-0-20" class="active">US sizes <br/> 0-20</th>
									<th data-table="#us-sizes-20-32">US sizes <br/> 23-32</th>
									<th data-table="#eu-sizes">EU sizes <br/> W / L</th>

								</tr>
							</thead>
						</table>
						<table width="100%" class="jeans-size" id="us-sizes-0-20">
							<tbody id="us-size">
								<tr>
									<th width="85px">Размер</th>
									<th>Inseam</th>
								</tr>
								<tr>
									<td class="size picked" width="85px">00</td>
									<td class="inseam">Extra short</td>
								</tr>
								<tr>
									<td class="size" width="85px">0</td>
									<td class="inseam picked">28"</td>
								</tr>
								<tr>
									<td class="size" width="85px">2</td>
									<td class="inseam">Short</td>
								</tr>
								<tr>
									<td class="size" width="85px">4</td>
									<td class="inseam">30"</td>
								</tr>
								<tr>
									<td class="size" width="85px">6</td>
									<td class="inseam" rowspan="2">Medium 32"</td>
								</tr>
								<tr>
									<td class="size" width="85px">8</td>
								</tr>
								<tr>
									<td class="size" width="85px">10</td>
									<td class="inseam" rowspan="2">Long 34"</td>
								</tr>
								<tr>
									<td class="size" width="85px">12</td>
								</tr>
								<tr>
									<td class="size" width="85px">14</td>
									<td class="inseam">Extra Long 36"</td>
								</tr>
								<tr>
									<td width="85px"></td>
									<td class="inseam">Super Long 38"</td>
								</tr>
								<tr>
									<td class="size" width="85px"></td>
									<td class="inseam"></td>
								</tr>
							</tbody>
						</table>
						<table width="100%" class="jeans-size" id="us-sizes-20-32">
							<tbody id="us-size">
								<tr>
									<th width="85px">Талия</th>
									<th>Inseam</th>
								</tr>
								<tr>
									<td class="size picked" width="85px">23</td>
									<td class="inseam" rowspan="2">Petite 28"</td>
								</tr>
								<tr>
									<td class="size" width="85px">24</td>
								</tr>

								<tr>
									<td class="size" width="85px">25</td>
									<td class="inseam" rowspan="2">Regular 30" </td>
								</tr>
								<tr>
									<td class="size" width="85px">26</td>
								</tr>

								<tr>
									<td class="size" width="85px">27</td>
									<td class="inseam" rowspan="2">Long 32" </td>
								</tr>
								<tr>
									<td class="size" width="85px">28</td>
								</tr>

								<tr>
									<td class="size" width="85px">29</td>
									<td class="inseam" rowspan="2">Long 34" </td>
								</tr>
								<tr>
									<td class="size" width="85px">30</td>
								</tr>

								<tr>
									<td class="size" width="85px">31</td>
									<td class="inseam" rowspan="2">Long 36" </td>
								</tr>
								<tr>
									<td class="size" width="85px">32</td>
								</tr>
							</tbody>
						</table>
						<table width="100%" class="jeans-size" id="eu-sizes">
							<tbody id="us-size">
								<tr>
									<th width="85px">Талия</th>
									<th>Inseam</th>
								</tr>

								<tr>
									<td class="size picked" width="85px">W24</td>
									<td class="inseam picked">L26</td>
								</tr>

								<tr>
									<td class="size" width="85px">W25</td>
									<td class="inseam">L27</td>
								</tr>
								<tr>
									<td class="size" width="85px">W26</td>
									<td class="inseam">L28</td>
								</tr>
								<tr>
									<td class="size" width="85px">W27</td>
									<td class="inseam">L30</td>
								</tr>
								<tr>
									<td class="size" width="85px">W28</td>
									<td class="inseam">L32</td>
								</tr>
								<tr>
									<td class="size" width="85px">W29</td>
									<td class="inseam">L34</td>
								</tr>
								<tr>
									<td class="size" width="85px">W30</td>
									<td class="inseam">L36</td>
								</tr>
								<tr>
									<td class="size" width="85px">W31</td>
									<td class="inseam">L38</td>
								</tr>
								<tr>
									<td class="size" width="85px">W32</td>

								</tr>
								<tr>
									<td class="size" width="85px">W33</td>

								</tr>
								<tr>
									<td class="size" width="85px">W34</td>
								</tr>

							</tbody>
						</table>

					</div>
				</div>
				<!-- Модальное окно с выбором размера-->
			</div>

	</li>
    <?php for ($i = 1; $i <= 8; $i++): ?>
    <li class="size-type custom-measure">
        <a href="#">
            <div class="inputfield" >
                <label><?php echo synved_option_get('options', 'row'.$i.'label') ?></label>
                <div class="bgactive">
                    <input type="text" class="inpfield required" name="<?php echo "OrderForm[row$i]" ?>" data-label="<?php echo synved_option_get('options', 'row'.$i.'label') ?>" value=""/>
                    <span class="b1"></span><span id="bg<?php echo $i ?>"></span></div>
                <div class="clear"></div>
            </div>
        </a>
    </li>
    <?php endfor; ?>
</ul>
<div class="panes">
<?php for ($i = 1; $i <= 8; $i++): ?>
<div>
    <div class="block-video">
        <div class="block-video-title"><span><?php echo synved_option_get('options', 'row'.$i.'header') ?></span></div>
        <div class="video">
            <iframe id="player" type="text/html" width="492" height="300"
                    src="<?php echo synved_option_get('options', 'row'.$i.'video') ?>"
                    frameborder="0"></iframe>
        </div>
    </div>
</div>
<?php endfor; ?>
</div>

<div class="price">Цена: <?php echo synved_option_get('options', 'price') ?> грн</div>
<div class="order-form">

        <!---div class="form-row">
            <input placeholder="Имя" class="required" type="text" name="name" data-label="Имя">
            <input placeholder="Телефон" class="required" type="text" name="phone" data-label="Телефон">
            <input placeholder="Email" class="required" type="text" name="email" data-label="Email">
        </div--->
        <!--div class="form-row">
            <textarea class="textarea" name="comments" data-label="Комментарии">комментарии к заказу...</textarea>
            <div class="clear"></div>
        </div-->
        <div class="form-row">
            <p>Прочитайте наши <a class="conditions" href="/conditions/">условия</a> работы</p>
            <input name="conditions" type="checkbox" class="required" data-label="Условия"/>
            <label>Я принимаю условия работы</label>
            <div class="clear"></div>
        </div>
        <div class="form-row-submit">
            <ul id="errors">
            </ul>
            <input type="submit" id="form-order" value="Оформить заказ"/>
            <div class="clear"></div>
        </div>

</div>

</div>
<div class="sidebar">
    <fieldset>
        <legend align="left">Модель</legend>
        <a href="/models/">
            <?php if(isset($_SESSION['model']['title'])): ?>
                <input type="hidden" class="required" name="OrderForm[model]" value="<?php echo $_SESSION['model']['title'] ?>">
                <?php echo $_SESSION['model']['title'] ?>
            <?php else: ?>
                <input type="hidden" class="required" name="model" value="">
                Выбрать модель
            <?php endif; ?>
        </a>
    </fieldset>
    <fieldset>
        <legend align="left">Тип посадки</legend>
        <p><input type="radio" class="required" name="OrderForm[r1]" value="Высокая" data-label="Тип посадки"/><label>Высокая</label></p>
        <p><input type="radio" name="OrderForm[r1]" value="Средняя" data-label="Тип посадки"/><label>Средняя</label></p>
        <p><input type="radio" name="OrderForm[r1]" value="Низкая" data-label="Тип посадки"/><label>Низкая</label></p>
    </fieldset>
    <fieldset>
        <legend align="left">Ткань</legend>
        <a href="/cloth/">
            <?php if(isset($_SESSION['cloth']['title'])): ?>
            <input type="hidden" class="required" name="OrderForm[cloth]" data-label="Ткань" value="<?php echo $_SESSION['cloth']['title'] ?>">
            <?php echo $_SESSION['cloth']['title'] ?>
            <?php else: ?>
            <input type="hidden" class="required" name="OrderForm[cloth]" data-label="Ткань" value="">
            Выбрать ткань
            <?php endif; ?>
        </a>
    </fieldset>
    <fieldset>
        <legend align="left">Нить</legend>
        <a href="/thread/">
            <?php if(isset($_SESSION['thread']['title'])): ?>
            <input type="hidden" name="OrderForm[cloth]" data-label="Нить" value="<?php echo $_SESSION['thread']['title'] ?>">
            <?php echo $_SESSION['thread']['title'] ?>
            <?php else: ?>
            <input type="hidden" class="required" name="OrderForm[cloth]" data-label="Нить" value="">
            Выбрать нить
            <?php endif; ?>
        </a>
    </fieldset>
	<fieldset>
		<legend align="left">Механические изменения</legend>
		<a href="/damages/">
			<?php if(isset($_SESSION['damages']['title'])): ?>
				<input type="hidden" name="OrderForm[damages]" data-label="Потёртость" value="<?php echo $_SESSION['damages']['title'] ?>">
				<?php echo $_SESSION['damages']['title'] ?>
			<?php else: ?>
				<input type="hidden" class="required" name="OrderForm[damages]" data-label="Потёртость" value="">
				Выбрать механичекие изменения
			<?php endif; ?>
		</a>
	</fieldset>
    <!--fieldset>
        <legend align="left">Механические изменения</legend>
        <p><input type="radio" class="required" name="r2" value="Базовые" data-label="Механические изменения"/><label>Базовые</label></p>
        <p><input type="radio" name="r2" value="Локальные" data-label="Механические изменения"/><label>Локальные</label></p>
    </fieldset-->
</div>
</form>

<!--script>
	$(function(){

	$('#form-order').click(function(e){
		e.preventDefault();
		return false;
		var errors = 0;
		$('#errors').html('');
		$('.error').removeClass('error');
		$('.required').each(function(){
			var val = $(this).val();
			var name = $(this).attr('name');
			if ($(this).attr('type') == 'radio') {
				if($('input[name='+name+']:checked').length > 0) {
					val = $('input[name='+name+']:checked').val();
				} else {
					val = '';
				}
			}

			if ($(this).attr('type') == 'checkbox') {
				if($('input[name='+name+']').attr('checked') == 'checked') {
					val = "yes";
				} else {
					val = '';
					$(this).parent().addClass('error');
				}
			}

			if (val.length == 0) {
				var label = $(this).data('label')
				if(!label)
					label = name;

				$(this).addClass('error');
				$(this).parents('fieldset').addClass('error');
				errors++;
				var error = '<li>Заполните обязательное поле ' + label + '</li>';
				$('#errors').append(error);
			}
		});

		if (errors > 0)
			return false;

	});
});
</script-->
<?php get_footer() ?>
